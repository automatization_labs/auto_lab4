import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StudentTest {

    @Test
    public void checkSetAge(){
        Student s1 = new Student("3","3","+380753434449", 18);
        s1.setAge(20);

        assertEquals(20, s1.getAge());
    }
}
