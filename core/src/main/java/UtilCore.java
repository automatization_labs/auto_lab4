import org.json.JSONObject;

import java.util.Random;

public class UtilCore {

    public static double generateRandomDouble(){
        Random r = new Random();
        return r.nextDouble();
    }

    public static String doSomethingWithJson(){
        JSONObject json = new JSONObject();
        json.put("key",generateRandomDouble());

        return json.toString();
    }
}
